﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistantDataManager : MonoBehaviour
{
    static public PersistantDataManager Instance;

    private int _score;

    private bool _isInitiated = false;

    public void Init() {
        Instance = this;

        _score = PlayerPrefs.GetInt("Score", 0);

        _isInitiated = true;
    }

    public int Score {
        get {
            if (!_isInitiated) {
                Init();
            }
            return _score;
        }
        set {
            _score = value;
            PlayerPrefs.SetInt("Score", _score);
        }
    }
}
