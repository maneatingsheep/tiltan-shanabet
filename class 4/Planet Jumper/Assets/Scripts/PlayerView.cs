﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerView : MonoBehaviour
{
    internal Rigidbody2D rigidBody;
    public Action<Collider2D> ImHit;


    public void Init() {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D planet) {
        ImHit(planet);
    }
}
