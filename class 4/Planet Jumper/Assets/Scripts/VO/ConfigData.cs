﻿using System;
using UnityEngine;

[Serializable]
public class ConfigData : MonoBehaviour {

    public float PlanetRangeXMin;
    public float PlanetRangeXMax;
    public float PlanetRangeGapYMin;
    public float PlanetRangeGapYMax;
    public float PlanetSizeMin;
    public float PlanetSizeMax;

    public float PlayerMass;
    public float PlayerRBMass;
    public float JumpForce;

    public string ToJson() {
        return JsonUtility.ToJson(this);

    }

    static public ConfigData FromJson(string json) {
        return JsonUtility.FromJson<ConfigData>(json);

    }
}
