﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowController : MonoBehaviour
{
    //model
    [SerializeField] private ConfigData _configData;
    [SerializeField] private PersistantDataManager _persistantDataManager;

    //controllers
    [SerializeField] private GameController _gameController;

    //view
    [SerializeField] private MainFlowView _mainFlowView;
    
    

    public enum FlowState { None, MainMenu, Settings, About, InGame };

    private FlowState _currentState = FlowState.None;


    #region Init
    void Start()
    {
        _persistantDataManager.Init();
        _gameController.Init(_configData);
        
        _mainFlowView.Init(_configData);


        _mainFlowView.OnButtonPressed += OnMenuButtonPressed;
        CurrentState = FlowState.MainMenu;


    }

    

    

    #endregion

    public FlowState CurrentState {
        get => _currentState;
        set {

            if (_currentState == value) return;

            _currentState = value;

            _mainFlowView.SetMenuState(_currentState);

            _gameController.SetActive(_currentState == FlowState.InGame);
           


            /*switch (_currentState)
            {
                case FlowState.MainMenu:
                    break;
                case FlowState.Settings:
                    break;
                case FlowState.About:
                    break;
                case FlowState.InGame:
                   
                    break;
                default:
                    Debug.LogError("Flow Statement is not valid!");
                    break;
            };*/
        }
    }

    

    

    private void OnMenuButtonPressed(MainFlowView.MMButtonType buttonType)
    {
       switch (buttonType)
        {
            case MainFlowView.MMButtonType.Start:
                CurrentState = FlowState.InGame;
                break;

            case MainFlowView.MMButtonType.Settings:
                CurrentState = FlowState.Settings;
                break;

            case MainFlowView.MMButtonType.About:
                CurrentState = FlowState.About;
                break;

        }
    }

}
