﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameView : MonoBehaviour
{

    public PlanetView PlanetPF;

    [SerializeField]
    private PlayerView _player;


    public Action<int> GotToPlanet;

    private float _cameraPosition;

    private List<PlanetView> _planets;
    private float _playerMass = 1f;
    private bool _isFloating;


    public void Init()
    {
        _player.Init();
        _player.ImHit += PlanetCollision;
        _planets = new List<PlanetView>();
        _isFloating = true;
    }

    public void ResetView()
    {
        _cameraPosition = 0;
        _isFloating = true;
    }



    internal void UpdateView(List<PlanetData> planetsData, int currentPlanet)
    {

        if (_planets.Count < planetsData.Count || _planets[_planets.Count - 1].Order < planetsData[planetsData.Count - 1].Order)
        {
            for (int i = 0; i < planetsData.Count; i++)
            {
                if (_planets.Count == 0 ||  planetsData[i].Order > _planets[_planets.Count - 1].Order)
                {
                    PlanetView planet = Instantiate<PlanetView>(PlanetPF);
                    planet.Init();
                    planet.Order = planetsData[i].Order;
                    planet.transform.position = new Vector2(planetsData[i].PositionX, planetsData[i].PositionY);
                    float scale = 6f + planetsData[i].Size * 2f;
                    planet.transform.localScale = new Vector2(scale, scale);
                    planet.Mass = planetsData[i].Size;
                    _planets.Add(planet);
                }
            }
        }
    }

    private void FixedUpdate() {

        if (!_isFloating || _planets == null) return;

        Vector2 AllPlanetForceVec = new Vector2();
        for (int i = 0; i < _planets.Count; i++) {
            Vector2 planetForceVec = _planets[i].transform.position - _player.transform.position;
            float distanceFromPlanet = planetForceVec.magnitude;
            float massesProduct = _planets[i].Mass * _playerMass;
            planetForceVec.Normalize();
            planetForceVec = planetForceVec * massesProduct / (distanceFromPlanet * distanceFromPlanet);
            AllPlanetForceVec += planetForceVec;
        }

        _player.rigidBody.AddForce(AllPlanetForceVec);
        _player.transform.rotation = Quaternion.Euler(0, 0, Vector2.Angle(Vector2.up, AllPlanetForceVec));

        Debug.DrawLine(_player.transform.position, _player.transform.position + (Vector3)AllPlanetForceVec);
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            Transform parentPlanet = _player.transform.parent;
            _player.transform.parent = this.transform;

            Vector2 positionDiff = _player.transform.position - parentPlanet.position;
            _player.rigidBody.AddForce(positionDiff * 3f, ForceMode2D.Impulse);
            _isFloating = true;
        }
    }

    private void PlanetCollision(Collider2D planetCollider) {
        for (int i = 0; i < _planets.Count; i++) {
            if (_planets[i].Collider == planetCollider) {
                _player.rigidBody.velocity = Vector2.zero;
                _player.transform.parent = _planets[i].transform;
                _isFloating = false;
                //stick  player to planet

                GotToPlanet(_planets[i].Order);

                break;
            }
        }
    }
}
