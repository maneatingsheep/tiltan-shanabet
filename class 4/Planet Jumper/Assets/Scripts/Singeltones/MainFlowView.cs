﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MainFlowView : MonoBehaviour
{    
    #region references

    [SerializeField] private Canvas MainMenuView; 
    [SerializeField] private Canvas SettingsView; 
    [SerializeField] private Canvas AboutView; 
    [SerializeField] private Canvas InGameView;

    #endregion    
    public enum MMButtonType {Start, Settings, About };

    public Action<MMButtonType> OnButtonPressed;

    public void Init(ConfigData configData) {

    }

    public void SetMenuState(FlowController.FlowState state)
    {
        MainMenuView.gameObject.SetActive(false);
        SettingsView.gameObject.SetActive(false);
        AboutView.gameObject.SetActive(false);
        InGameView.gameObject.SetActive(false);

        switch (state)
        {
            case FlowController.FlowState.MainMenu:
                MainMenuView.gameObject.SetActive(true);
                break;
            case FlowController.FlowState.Settings:
                SettingsView.gameObject.SetActive(true);
                break;
            case FlowController.FlowState.About:
                AboutView.gameObject.SetActive(true);
                break;
            case FlowController.FlowState.InGame:
                InGameView.gameObject.SetActive(true);
                break;
            default:
                Debug.LogError("Flow Statement is not valid!");
                break;
        };
    }

    public void StartButtonPressed()
    {
        OnButtonPressed(MMButtonType.Start);
    }

    public void SettingsButtonPressed()
    {
        OnButtonPressed(MMButtonType.Settings);        
    }

    public void AboutButtonPressed()
    {
        OnButtonPressed(MMButtonType.About);
    }

}
