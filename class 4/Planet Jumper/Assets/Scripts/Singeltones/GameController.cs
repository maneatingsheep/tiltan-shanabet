﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private GameView _gameView;

    private const int PLANETS_FORWARD = 10;
    private const int PLANETS_BACKWARDS = 2;

    private float PlanetRangeXMin;
    private float PlanetRangeXMax;
    private float PlanetRangeGapYMin;
    private float PlanetRangeGapYMax;
    private float PlanetSizeMin;
    private float PlanetSizeMax;


    private int _currentPlanet;
    private List<PlanetData> _planets;


    public void Init(ConfigData configData)
    {

        PlanetRangeXMin = configData.PlanetRangeXMin;
        PlanetRangeXMax = configData.PlanetRangeXMax;
        PlanetRangeGapYMin = configData.PlanetRangeGapYMin;
        PlanetRangeGapYMax = configData.PlanetRangeGapYMax;
        PlanetSizeMin = configData.PlanetSizeMin;
        PlanetSizeMax = configData.PlanetSizeMax;


        _planets = new List<PlanetData>();



        _gameView.Init();
        _gameView.GotToPlanet += PlanetReached;
    }

    

    private void ResetGame()
    {
        PersistantDataManager.Instance.Score = 0;
        _currentPlanet = 0;

        _planets.Clear();

        FillinPLanets();
    }


    private void FillinPLanets()
    {
        //add planets ahead
        while (_planets.Count == 0 || _planets[_planets.Count - 1].Order < _currentPlanet + PLANETS_FORWARD)
        {

            int previousOrder = -1;
            float previousYposition = -1;
            
            if (_planets.Count > 0)
            {
                previousOrder = _planets[_planets.Count - 1].Order;
                previousYposition = _planets[_planets.Count - 1].PositionY;
            }

           _planets.Add(GeneratePlanet(previousOrder + 1, previousYposition));
        }

        //remove tail
        while (_planets[0].Order < _currentPlanet - PLANETS_BACKWARDS)
        {
            _planets.RemoveAt(0);
        }


        _gameView.UpdateView(_planets, _currentPlanet);
    }

    private PlanetData GeneratePlanet(int order, float previousPositionY)
    {
        //planet random generation
        PlanetData retVal = new PlanetData() {
            Order = order,
            Size = UnityEngine.Random.Range(PlanetSizeMin, PlanetSizeMax),
            PositionX = (order == 0) ? 0 : (UnityEngine.Random.Range(PlanetRangeXMin, PlanetRangeXMax)),
            PositionY = (order == 0) ? 0 : (previousPositionY + (UnityEngine.Random.Range(PlanetRangeGapYMin, PlanetRangeGapYMax)))
        };

        return retVal;

    }

    internal void SetActive(bool isGame)
    {
        if (isGame)
        {
            ResetGame();

        }

        _gameView.gameObject.SetActive(isGame);
    }


    private void PlanetReached(int planetOrder) {
        if (planetOrder > _currentPlanet) {
            PersistantDataManager.Instance.Score++;

        }
        _currentPlanet = planetOrder;
    }

}

public struct PlanetData
{
    public int Order;
    public float Size;

    public float PositionX;
    public float PositionY;
}
