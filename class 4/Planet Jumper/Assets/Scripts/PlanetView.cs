﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetView : MonoBehaviour
{
    public int Order;
    public float Mass;
    public Collider2D Collider;

    public void Init() {
        Collider = GetComponent<Collider2D>();
    }

    private void Update() {
        transform.Rotate(0, 0, 36f * Time.deltaTime);
    }

    
}
